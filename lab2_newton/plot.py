import numpy as np
import matplotlib.pyplot as plt
import math

f1 = lambda x: 1 - math.sin(x) / 2
f2 = lambda x: math.acos(0.7 - 2 * x) + 1

x = np.linspace(-0.15, 0.85, 300)


plt.xlabel("x")         # ось абсцисс
plt.ylabel("y")    # ось ординат
plt.grid()              # включение отображение сетки

plt.plot(x, [f1(arg) for arg in x], label="sin(x) + 2y = 2")  # построение графика
plt.plot(x, [f2(arg) for arg in x], label="2x + cos(y − 1) = 0.7")

plt.legend()

plt.show()
