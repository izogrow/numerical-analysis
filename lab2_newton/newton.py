import numpy as np
from math import sin, cos, fabs
import matplotlib.pyplot as plt
import math


def plot():
    f1 = lambda x: 1 - sin(x) / 2
    f2 = lambda x: math.acos(0.7 - 2 * x) + 1

    x = np.linspace(-0.15, 0.85, 500)
    x2 = np.linspace(-5, 5, 500)

    plt.xlabel("x")  # ось абсцисс
    plt.ylabel("y")  # ось ординат
    plt.grid()  # включение отображение сетки

    plt.plot(x2, [f1(arg) for arg in x2], label="sin(x) + 2y = 2")  # построение графика
    plt.plot(x, [f2(arg) for arg in x], label="2x + cos(y − 1) = 0.7")

    plt.legend()

    plt.show()


def func1(x, y):
    return sin(x) + 2 * y - 2


def func2(x, y):
    return 2 * x + cos(y - 1) - 0.7


F = np.array([[func1], [func2]])

W = np.array([
    [lambda x, y: cos(x), lambda x, y: 2],     # Матрица Якоби
    [lambda x, y: 2, lambda x, y: -sin(y)]
])


def F(x, y):
    return np.array([[func1(x, y)], [func2(x, y)]])


def inverseW(xin: float, yin: float):
    return np.array([
        [(lambda x, y: sin(y)/(cos(x) * sin(y) + 4))(xin, yin), (lambda x, y: 2/(cos(x) * sin(y) + 4))(xin, yin)],
        [(lambda x, y: 2/(cos(x) * sin(y) + 4))(xin, yin), (lambda x, y: -cos(x)/(cos(x) * sin(y) + 4))(xin, yin)]
    ])


def newton(x0, y0, e):
    i = 0
    [[x1], [y1]] = np.array([[x0], [y0]]) - inverseW(x0, y0).dot(F(x0, y0))
    x_old = 0
    while True:

        if fabs(x1 - x0) <= e and fabs(y1 - y0) <= e:
            return x1, y1, i
        if fabs(x1 - x0) > fabs(x0 - x_old) and fabs(y1 - y0) > fabs(y0 - y_old) and i != 0:
            return None, None, None

        x_old = x0
        y_old = y0
        x0 = x1
        y0 = y1
        [[x1], [y1]] = np.array([[x0], [y0]]) - inverseW(x0, y0).dot(F(x0, y0))
        i += 1


def newton_mod(x_0, y_0, e):
    i = 0
    [[x1], [y1]] = np.array([[x_0], [y_0]]) - inverseW(x_0, y_0).dot(F(x_0, y_0))
    x0 = x_0
    y0 = y_0

    x_old = 0
    while True:
        if fabs(x1 - x0) <= e and fabs(y1 - y0) <= e:
            return x1, y1, i
        if fabs(x1 - x0) > fabs(x0 - x_old) and fabs(y1 - y0) > fabs(y0 - y_old) and i != 0:
            return None, None, None
        x_old = x0
        y_old = y0
        x0 = x1
        y0 = y1
        [[x1], [y1]] = np.array([[x0], [y0]]) - inverseW(x_0, y_0).dot(F(x0, y0))
        i += 1


def main():
    plot()
    e = 0.00001
    print("sin(x) + 2 * y = 2")
    print("2 * x + cos(y - 1) = 0.7")
    print()

    print("Начальное приближение:")
    x = float(input("Введите x>"))  # Начальное приближение
    y = float(input("Введите y>"))

    x1, y1, i1 = newton(x, y, e)
    if x1 is None:
        print("Метод Ньютона не сходится.")
    else:
        print("Метод Ньютона:")
        print(f"x = {x1}, y = {y1}, Количество итераций: {i1}, Точность: {e}")

    x2, y2, i2 = newton_mod(x, y, e)
    if x2 is None:
        print("Модифицированный метод Ньютона не сходится.")
    else:
        print("Модифицированный метод Ньютона:")
        print(f"x = {x2}, y = {y2}, Количество итераций: {i2}, Точность: {e}")


main()
