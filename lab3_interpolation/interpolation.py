from math import pi, fabs, factorial
import numpy as np
import matplotlib.pyplot as plt


def func(x):
    return 5 * x * ((5 * pi + 2 * x) ** (-1 / 4))


def find_max(func, x1, x2, eps):
    while fabs(x1 - x2) >= eps:
        y1 = fabs(func(((x1 + x2) / 2) - eps))
        y2 = fabs(func(((x1 + x2) / 2) + eps))
        if y1 > y2:
            x2 = x1 + ((x2 - x1) / 2) - eps
        else:
            x1 = x1 + ((x2 - x1) / 2) + eps
    else:
        return (x1 + x2) / 2


def lagrange(x, y, arg):
    z = 0
    for j in range(len(y)):
        p1 = 1
        p2 = 1
        for i in range(len(x)):
            if i == j:
                pass
            else:
                p1 *= arg - x[i]
                p2 *= x[j] - x[i]
        z += y[j] * p1 / p2
    return z


def newton(a, b, arg):
    x = np.linspace(a, b, num=4)
    y = [func(u) for u in np.linspace(1, 3, num=4)]
    dy = [y[1] - y[0], y[2] - y[1], y[3] - y[2]]
    d2y = [dy[1] - dy[0], dy[2] - dy[1]]
    d3y = d2y[1] - d2y[0]
    dy_first_row = [dy[0], d2y[0], d3y]

    s = y[0]
    w = 1
    i = 1
    h = x[1] - x[0]
    t = (arg - x[0]) / h
    for diy in dy_first_row:
        w *= t - i + 1

        s += diy * w / factorial(i)
        i += 1
    return s


def plot(a, b):
    plt.grid()
    space_all = np.linspace(0, 6, num=20)
    plt.plot(space_all, [func(arg) for arg in space_all], label="5 * x * ((5 * pi + 2 * x) ^ (-1 / 4))")

    space = np.linspace(a, b, num=100)
    x = np.linspace(a, b, num=4)
    y = [func(arg) for arg in x]

    plt.plot(space, [lagrange(x, y, arg) for arg in space], label="Многочлен Лагранжа")
    plt.legend()
    plt.title("Исходная функция")
    plt.show()


def plot_err(a, b):
    plt.grid()
    space = np.linspace(a, b, num=100)
    x = np.linspace(a, b, num=4)
    y = [func(arg) for arg in x]

    plt.plot(space, [fabs(func(arg) - lagrange(x, y, arg)) for arg in space], label="Абсолютная погрешность")
    plt.legend()
    plt.show()


def plot_lagrange(a, b):
    plt.grid()
    space = np.linspace(a, b, num=100)
    x = np.linspace(a, b, num=4)
    y = [func(arg) for arg in x]

    plt.plot(space, [lagrange(x, y, arg) for arg in space], label="Многочлен Лагранжа")
    plt.legend()
    plt.show()


plot(1, 3)

a = 0
b = 6

e = 1
EPS = 0.001

while e > EPS:
    a += 0.01
    b -= 0.01
    x = np.linspace(a, b, num=4)


    def M_4(arg):
        return -84.375 * ((5 * pi + 2 * arg) ** (-13 / 4)) - 182.8125 * ((5 * pi + 2 * arg) ** (-17 / 4))


    def w_4(arg):
        return (arg - x[0]) * (arg - x[1]) * (arg - x[2]) * (arg - x[3])

    e = find_max(M_4, a, b, 0.001) * find_max(w_4, a, b, 0.001) / (2 * 3 * 4)
    print("e = ", e, "a = ", a, "b =", b)

else:
    print("Отрезок с нужной точностью найден:")
    print("[ " + str(a) + ", " + str(b) + " ]")
    print()

    print("Строим многочлен Лагранжа на этом отрезке и проверяем его абсолютную погрешность.")
    print("Из графика видно, что погрешность в некоторых участках превышает 10^-3. Оценка погрешности нас подвела.")
    # plot_err(a, b)

    print("Попытаемся сузить вручную, построив полином Лагранжа на [ 1, 3 ].")
    print("Проверим график абсолютной погрешности.")
    print("Отлично! Погрешность не превышает 0.000051, этот отрезок нам подходит.")
    print()
    a = 1
    b = 3
    plot_err(a, b)
 #   plot_lagrange(a, b)

    print("Теперь построим многочлен Ньютона.")

    x_i = input("Ввдедите x в пределах [" + str(a) + ", " + str(b) + "] для проверки>")
    x = np.linspace(a, b, num=4)
    print("Вычисление исходной функции:       ", func(float(x_i)))
    print("Вычисление по многочлену Лагранжа: ", lagrange(x, [func(arg) for arg in x], float(x_i)))
    print("Вычисление по многочлену Ньютона:  ", newton(1, 3, float(x_i)))

