import numpy as np
import matplotlib.pyplot as plt
from math import pi, fabs, factorial, sqrt


def f(x):
    return 1 / (x ** 2 + x)


a = 1
b = 3
e = 0.001
# e = 0.000001


def f_div_2(x):
    return 2 * (2 * x + 1) ** 2 / (x ** 2 + x) ** 3 - 2 / (x ** 2 + x) ** 2


def f_div_4(x):
    return 24 * (2 * x + 1) ** 4 / (x ** 2 + x) ** 5 - 72 * (2 * x + 1) ** 2 / (x ** 2 + x) ** 4 + 24 / (x ** 2 + x) ** 3


def M_2():
    return find_max(f_div_2, a, b, e)


def M_4():
    return find_max(f_div_4, a, b, e)


def find_max(func, x1, x2, eps):
    while fabs(x1 - x2) >= eps:
        y1 = fabs(func(((x1 + x2) / 2) - eps))
        y2 = fabs(func(((x1 + x2) / 2) + eps))
        if y1 > y2:
            x2 = x1 + ((x2 - x1) / 2) - eps
        else:
            x1 = x1 + ((x2 - x1) / 2) + eps
    else:
        return (x1 + x2) / 2


def integral_trapezoid(n):
    sum_f = 0
    n = int(n + 1)

    while n % 4 != 0:
        n += 1

    space = np.linspace(a, b, num=(n + 1))
    h = (b - a) / n
    print("Шаг интегрирования:", h)
    print("n = ", n)
    for x in space:
        sum_f += f(x)
    sum_f -= f(a)
    I = h * ((f(a) + f(b)) / 2 + sum_f)

    print("Интеграл по формуле трапеций с шагом h:", I)
    print()

    n = int(n / 2 + 1)
    while n % 4 != 0:
        n += 1

    space = np.linspace(a, b, num=(n + 1))
    h = (b - a) / n
    print("Шаг интегрирования:", h)
    print("n = ", n)
    sum_f = 0
    for x in space:
        sum_f += f(x)
    sum_f -= f(a)
    I2 = h * ((f(a) + f(b)) / 2 + sum_f)


    print("Интеграл по формуле трапеций с шагом 2h:", I2)

    err = fabs((I - I2) / 3)
    print("Уточненная оценка погрешности:", err)
    print()
    return I


def integral_simpson(n):
    sum_f1 = 0
    sum_f2 = 0

    n = int(n + 1)

    while n % 4 != 0:
        n += 1

    space = np.linspace(a, b, num=(n + 1))
    h = (b - a) / n
    print("Шаг интегрирования:", h)
    print("n = ", n)
    for i in range(1, space.size):
        sum_f1 += f((space[i - 1] + space[i]) / 2)

    for x in space:
        sum_f2 += f(x)
    sum_f2 -= f(a)
    sum_f2 -= f(b)

    I = h / 6 * (f(a) + f(b) + 4 * sum_f1 + 2 * sum_f2)

    print("Интеграл по формуле Симпсона с шагом h:", I)

    sum_f1 = 0
    sum_f2 = 0

    n = int(n / 2)

    while n % 4 != 0:
        n += 1

    space = np.linspace(a, b, num=(n + 1))
    h = (b - a) / n
    print("Шаг интегрирования:", h)
    print("n = ", n)
    for i in range(1, space.size):
        sum_f1 += f((space[i - 1] + space[i]) / 2)

    for x in space:
        sum_f2 += f(x)
    sum_f2 -= f(a)
    sum_f2 -= f(b)

    I2 = h / 6 * (f(a) + f(b) + 4 * sum_f1 + 2 * sum_f2)

    print("Интеграл по формуле Симпсона с шагом 2h:", I2)

    err = fabs((I - I2) / 3)
    print("Уточненная оценка погрешности:", err)
    return I


if __name__ == '__main__':
    h_tr = sqrt(12 * e / (M_2() * (b - a)))
    n_tr = (b - a) / h_tr

    integral_trapezoid(n_tr)

    h_s = sqrt(sqrt(2880 * e / (M_4() * (b - a))))
    n_s = (b - a) / h_s

    integral_simpson(n_tr)
    print()
    print("Значение, полученное по формуле Ньютона-Лейбница:", 0.405465108)
    print("Сравнивая это значение со значениями, полученными с момощью методов трапеций и Симпсона,")
    print("увидим, что более точный результат показывает метод Симпсона.")


