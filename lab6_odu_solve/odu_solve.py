import numpy as np
import matplotlib.pyplot as plt
from math import fabs, sqrt, log


a = 1
b = 5

EPS = 10 ** -4

h0 = sqrt(sqrt(EPS))

x0 = a
y0 = 1


def f(x, y):
    return (y ** 2) / 3 - y / x


def f_runge_kutta(x_n, y_n, h):
    k1 = f(x_n, y_n)
    k2 = f(x_n + h / 2, y_n + h / 2 * k1)
    k3 = f(x_n + h / 2, y_n + h / 2 * k2)
    k4 = f(x_n + h, y_n + h * k3)
    return y_n + h / 6 * (k1 + 2 * k2 + 2 * k3 + k4)


def f_euler(x_n, y_n, h):
    return y_n + h * f(x_n, y_n)


def f_exact_solution(x):
    return 3 / (x * (3 - log(fabs(x))))


def solve(func, h, n):
    x_space = np.linspace(a, b, num=(n + 1))
    y_space = [y0]
    y_space2 = [y0]
    delta_y = []

    i = 0
    for x in x_space:
        y_space.append(func(x, y_space[i], h))
        i += 1
    y_space.pop(-1)

    j = 0
    for i in range(0, x_space.size, 2):
        y_space2.append(func(x_space[i], y_space2[j], 2 * h))
        delta_y.append(fabs(y_space[i] - y_space2[j]))
        j += 1

    y_space2.pop(-1)

    max_delta_y = np.max(delta_y)

    print()
    print(func.__name__, ":")
    print()
    print("max_delta_y =" + str(max_delta_y))

    print("\n   x\t   y_i_h\ty_i_2h\t delta y")
    for i in range(len(x_space)):
        if i % 2 == 0:
            print("%8.5f %8.5f %8.5f %8.5f" % (x_space[i], y_space[i], y_space2[i // 2], delta_y[i // 2]))
        else:
            print("%8.5f %8.5f" % (x_space[i], y_space[i]))

    return y_space, delta_y


def find_h_and_n(h0):
    h = h0
    print("h0 =", h)

    y1 = f_runge_kutta(x0, y0, h)
    x1 = x0 + h
    print("y1 =", y1)

    y2 = f_runge_kutta(x1, y1, h)
    print("y2 с шагом h: ", y2)
    y2_2 = f_runge_kutta(x0, y0, 2 * h)
    print("y2 с шагом 2h:", y2_2)
    print("|y2 - y2_2| =", fabs(y2 - y2_2))
    print()

    if fabs(y2 - y2_2) >= EPS:
        print("|y2 - y2_2| >", EPS)

        h = h0 * 2
        while fabs(y2 - y2_2) >= EPS:
            print("Уменьшим шаг в 2 раза")
            h = h / 2

            y1 = f_runge_kutta(x0, y0, h)
            x1 = x0 + h
            print("y1 =", y1)

            y2 = f_runge_kutta(x1, y1, h)
            print("y2 с шагом h: ", y2)
            y2_2 = f_runge_kutta(x0, y0, 2 * h)
            print("y2 с шагом 2h:", y2_2)
            print("|y2 - y2_2| =", fabs(y2 - y2_2))
            print()
    else:
        print("|y2 - y2_2| <", EPS)
        while fabs(y2 - y2_2) <= EPS:
            print("Увеличим шаг в 2 раза")
            h = h * 2
            print("h =", h)

            y1 = f_runge_kutta(x0, y0, h)
            x1 = x0 + h
            print("y1 =", y1)

            y2 = f_runge_kutta(x1, y1, h)
            print("y2 с шагом h: ", y2)
            y2_2 = f_runge_kutta(x0, y0, 2 * h)
            print("y2 с шагом 2h:", y2_2)
            print("|y2 - y2_2| =", fabs(y2 - y2_2))
            print()

    n = int((b - a) / h)

    if n % 2 == 1:
        print("n =", n, ", увеличим на 1, сделав четным.")
        n += 1

        h = (b - a) / n
        print("h0 =", h)

        y1 = f_runge_kutta(x0, y0, h)
        x1 = x0 + h
        print("y1 =", y1)

        y2 = f_runge_kutta(x1, y1, h)
        print("y2 с шагом h: ", y2)
        y2_2 = f_runge_kutta(x0, y0, 2 * h)
        print("y2 с шагом 2h:", y2_2)
        print("|y2 - y2_2| =", fabs(y2 - y2_2))
        print()

        while (fabs(y2 - y2_2) >= EPS):
            print("Уменьшим шаг в 2 раза")
            h = h / 2
            print("h =", h)

            y1 = f_runge_kutta(x0, y0, h)
            x1 = x0 + h
            print("y1 =", y1)

            y2 = f_runge_kutta(x1, y1, h)
            print("y2 с шагом h: ", y2)
            y2_2 = f_runge_kutta(x0, y0, 2 * h)
            print("y2 с шагом 2h:", y2_2)
            print("|y2 - y2_2| =", fabs(y2 - y2_2))
            print()
        n = int((b - a) / h)

    return h, n


if __name__ == '__main__':
    h, n = find_h_and_n(h0)
    print("Шаг найден:", h)
    print("Количество отрезков:", n)

    y_runge_kutta, y_delta_runge_kutta = solve(f_runge_kutta, h, n)
    y_euler, y_delta_euler = solve(f_euler, h, n)

    x_space = np.linspace(a, b, num=(n + 1))
    y_exact_solution = [f_exact_solution(arg) for arg in x_space]

    y_delta = np.abs(np.subtract(y_exact_solution, y_runge_kutta))
    max_delta_y = np.max(y_delta)
    print()
    print("Точное решение:")
    print()
    print("Максимум отклонения приближенного значения от точного:", max_delta_y)

    print("\n   x\t   y\ty_runge_kutta delta y")
    for i in range(len(x_space)):
        print("%8.6f %8.6f %8.6f %8.6f" % (x_space[i], y_exact_solution[i], y_runge_kutta[i], y_delta[i]))

    plt.plot(x_space, y_runge_kutta, label="Runge-Kutta")
    plt.plot(x_space, y_euler, label="Euler")
    plt.plot(x_space, y_exact_solution, label="f(x)", linestyle="--", color="red")
    plt.legend()
    plt.show()
