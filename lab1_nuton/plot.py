import pylab
import numpy

func_glob = lambda x: x ** 3 - 3.3 * x ** 2 - 1.6 * x + 1.7

X = numpy.arange(-2.0, 4.0, 0.1)
pylab.plot([x for x in X], [func_glob(x) for x in X])
pylab.grid(True)
pylab.show()
