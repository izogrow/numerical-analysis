# plot = x**3 - 3.3 * x**2 - 1.6 * x + 1.7
import math


def func_glob(x):
    return x ** 3 - 3.3 * x ** 2 - 1.6 * x + 1.7


def func_first(x):
    return 3 * x ** 2 - 6.6 * x - 1.6


print("Введите границы для метода половинного деления>")
a1 = int(input())
b1 = int(input())

e = 0.001


def half_divide_method(a, b, f):
    i = 0
    x = (a + b) / 2
    while math.fabs(f(x)) > 2 * e:
        i += 1
        x = (a + b) / 2
        a, b = (a, x) if f(a) * f(x) < 0 else (x, b)
    return (a + b) / 2, i


def newtons_method(x0, f, f1):
    i = 0
    x1 = x0 - (f(x0) / f1(x0))
    while True:
        if math.fabs(x1 - x0) <= e:
            return x1, i
        i += 1
        x0 = x1
        x1 = x0 - (f(x0) / f1(x0))


ans_div, i = half_divide_method(a1, b1, func_glob)
print('Решение по методу половинного деления: %s' % ans_div)
print('Количество итераций: ', i)
print('f(x*) = ', func_glob(ans_div))

print('Введите начальное приближение для метода Ньютона>')
c = int(input())

ans_newt, i = newtons_method(c, func_glob, func_first)
print('Решение по методу Ньютона: %s' % ans_newt)
print('Количество итераций: ', i)
print('f(x*) = ', func_glob(ans_newt))
