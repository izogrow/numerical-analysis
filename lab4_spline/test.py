a = [1, 2, 3]
b = [4, 5, 6]
c = [7, 8, 9]

# a, b, c = map(lambda k_list: map(float, k_list), (a, b, c))
#
# print(a[1])

l = [1, 2, 3]
l.append([4, 5])
print(l)
l.extend([4, 5])
print(l)

g = [0] * 5
print(g)
