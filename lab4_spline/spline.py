import numpy as np
import matplotlib.pyplot as plt
from math import pi, fabs, factorial


def func(x):
    return 5 * x * ((5 * pi + 2 * x) ** (-1 / 4))


def newton(arg):
    a = 1
    b = 3
    x = np.linspace(a, b, num=4)
    y = [func(u) for u in np.linspace(1, 3, num=4)]
    dy = [y[1] - y[0], y[2] - y[1], y[3] - y[2]]
    d2y = [dy[1] - dy[0], dy[2] - dy[1]]
    d3y = d2y[1] - d2y[0]
    dy_first_row = [dy[0], d2y[0], d3y]

    s = y[0]
    w = 1
    i = 1
    h = x[1] - x[0]
    t = (arg - x[0]) / h
    for diy in dy_first_row:
        w *= t - i + 1

        s += diy * w / factorial(i)
        i += 1
    return s


def linear_spline(arg):
    x1 = 1
    x2 = 3

    x = np.linspace(x1, x2, num=4)

    if x[0] <= arg <= x[1]:
        a1 = func(x[1])
        b1 = (func(x[0]) - func(x[1])) / (x[0] - x[1])
        return a1 + b1 * (arg - x[1])
    elif x[1] < arg <= x[2]:
        a2 = func(x[2])
        b2 = (func(x[1]) - func(x[2])) / (x[1] - x[2])
        return a2 + b2 * (arg - x[2])
    elif x[2] < arg <= x[3]:
        a3 = func(x[3])
        b3 = (func(x[2]) - func(x[3])) / (x[2] - x[3])
        return a3 + b3 * (arg - x[3])


def get_parabolic_spline(x_val: (np.ndarray, list), y_val: (np.ndarray, list), h: float):
    A = np.asarray([[1, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 0, 0, 0, 0],
                    [1, 0, 0, h, 0, 0, h * h, 0, 0],
                    [0, 1, 0, 0, h, 0, 0, h * h, 0],
                    [0, 0, 1, 0, 0, h, 0, 0, h * h],
                    [0, 0, 0, 1, -1, 0, 2 * h, 0, 0],
                    [0, 0, 0, 0, 1, -1, 0, 2 * h, 0],
                    [0, 0, 0, 0, 0, 0, 2, 0, 0]])
    B = [y_val[i] for i in range(3)]
    B.extend([y_val[i] for i in range(1, 4)])
    B.extend([0] * 3)
    B = np.asarray(B)

    # Решаем систему (СЛАУ)
    coef = np.linalg.solve(A, B).ravel()

    x_0 = x_val[0]
    c_step = 3

    del B
    del A
    del x_val

    def f(x: float) -> float:
        index = int((x - x_0) / h)
        x_sub_x_i = x - index * h - x_0

        if index < 0:
            return y_val[0]
        if index > len(y_val) - 2:
            return y_val[len(y_val) - 1]

        return coef[index] + coef[index + c_step] * x_sub_x_i + coef[index + 2 * c_step] * x_sub_x_i ** 2

    return f


inter_space = np.linspace(1, 3, num=4)
parabolic_spline_build_in = get_parabolic_spline(inter_space, [func(x) for x in inter_space], inter_space[1] - inter_space[0])


def parabolic_spline(arg):
    x1 = 1
    x2 = 3

    x_interpolation = np.linspace(x1, x2, num=4)

    x = [x1]
    x_half = h = (x_interpolation[1] - x_interpolation[0]) / 2  # = h
    x.extend(np.linspace(x1 + x_half, x2 - x_half, num=3))  # Узлы сплайна будут лежать между узлами интерполяции
    x.append(x2)

    c = [0] * 4

    x = x_interpolation

    delta = -1/6
    lambda_var = [0, 0, (func(x[2]) - 2 * func(x[1]) + func(x[0])) / (6 * h ** 2)]
    for n in range(2, 3):
        lambda_var.append((((func(x[n+1]) - 2 * func(x[n]) + func(x[n-1])) / h ** 2) - lambda_var[n]) / (delta + 6))

    for n in range(2, 1, -1):  # или (3, 0, -1)
        c[n] = delta * c[n+1] + lambda_var[n+1]

    # h = [0]  # h = x[i] - x[i - 1]
    # for i in range(1, 5):
    #     h.append(x[i] - x[i - 1])

    b_mid = [(h * (c[k] - c[k+1]) / 2) + (func(x[k]) - func(x[k-1])) / (2 * h) for k in range(1, len(x) - 1)]
    b = [0]
    b.extend(b_mid)
    b.append(b[-1])

    a_mid = [h * (b[k] - h * c[k]) + func(x[k-1]) for k in range(1, len(x) - 1)]
    a = [0]
    a.extend(a_mid)
    a.append(func(x[-1]))
    # a = [func(x) for x in x]

    if x[0] <= arg <= x[1]:
        return a[1] + b[1] * (arg - x[1]) + c[1] * (arg - x[1]) ** 2
    elif x[1] < arg <= x[2]:
        return a[2] + b[2] * (arg - x[2]) + c[2] * (arg - x[2]) ** 2
    elif x[2] < arg <= x[3]:
        return a[3] + b[3] * (arg - x[3]) + c[3] * (arg - x[3]) ** 2


def parabolic_spline_old(arg):
    x1 = 1
    x2 = 3

    x = np.linspace(x1, x2, num=4)

    a = [func(x) for x in x]   # здесь мы также находим a0 для того, чтобы найти b

    h = [0]  # h = x[i] - x[i - 1]

    for i in range(1, 4):
        h.append(x[i] - x[i - 1])

    b = [0, 0, 0, 0]

    for i in range(3, 0, -1):
        b[i-1] = (2 * (a[i] - a[i-1])) / h[i] - b[i]

    c = [0, 0, 0, 0]

    for i in range(3, 0, -1):
        c[i] = (b[i] - b[i-1]) / (2 * h[i])

   # c[1] = b[1] / (2 * h[1])
   # c[1] = (a[1] - a[0]) / h[1]**2

    if x[0] <= arg <= x[1]:
        return a[1] + b[1] * (arg - x[1]) + c[1] * (arg - x[1]) ** 2
    elif x[1] < arg <= x[2]:
        return a[2] + b[2] * (arg - x[2]) + c[2] * (arg - x[2]) ** 2
    elif x[2] < arg <= x[3]:
        return a[3] + b[3] * (arg - x[3]) + c[3] * (arg - x[3]) ** 2


def cubic_spline(arg):
    x1 = 1
    x2 = 3

    x = np.linspace(x1, x2, num=4)

    a = [func(x[0]), func(x[1]), func(x[2]), func(x[3])]  # здесь мы также находим a0 для того, чтобы найти c
    c = [0, 0, 0, 0]
    h = [0]  # h = x[i] - x[i - 1]

    for i in range(1, 4):
        h.append(x[i] - x[i - 1])

    delta = [0, -h[2] / (2 * h[1] + 2 * h[2])]  # метод прогонки
    lambda_var = [0, (3 * (a[2] - a[1]) / (x[2] - x[1]) - 3 * (a[1] - a[0]) / (x[1] - x[0])) / (2 * h[1] + 2 * h[2])]
    for k in range(3, 4):
        delta.append(-h[k] / (2 * h[k-1] + 2 * h[k] + h[k-1] * delta[k-2]))
        lambda_var.append((3 * (a[k] - a[k-1]) / (x[k] - x[k-1]) - 3 * (a[k-1] - a[k-2]) / (x[k-1] - x[k-2]) - h[k-1] * lambda_var[k-2]) / (2 * h[k-1] + 2 * h[k] + h[k-1] * delta[k-2]))

    for k in range(3, 1, -1):
        c[k-1] = delta[k-1] * c[k] + lambda_var[k-1]

    b = [0]
    d = [0]
    for i in range(1, 4):
        d.append((c[i] - c[i - 1]) / (3 * h[i]))
        b.append((a[i] - a[i - 1]) / h[i] + h[i] * (2 * c[i] + c[i - 1]) / 3)

    if x[0] <= arg <= x[1]:
        return a[1] + b[1] * (arg - x[1]) + c[1] * (arg - x[1]) ** 2 + d[1] * (arg - x[1]) ** 3
    elif x[1] < arg <= x[2]:
        return a[2] + b[2] * (arg - x[2]) + c[2] * (arg - x[2]) ** 2 + d[2] * (arg - x[2]) ** 3
    elif x[2] < arg <= x[3]:
        return a[3] + b[3] * (arg - x[3]) + c[3] * (arg - x[3]) ** 2 + d[3] * (arg - x[3]) ** 3


def cubic_spline_old(arg):
    x1 = 1
    x2 = 3

    x = np.linspace(x1, x2, num=4)

    a = [func(x[0]), func(x[1]), func(x[2]), func(x[3])]  # здесь мы также находим a0 для того, чтобы найти c
    c = [0]
    h = [0]     # h = x[i] - x[i - 1]

    for i in range(1, 4):
        h.append(x[i] - x[i - 1])

    c.append((3 * ((a[2] - a[1]) / h[2] - (a[1] - a[0]) / h[1]) - 3 * h[2] * ((a[3] - a[2]) / h[3] - (a[2] - a[1]) / h[2]) / (2 * (h[2] + h[3]))) / (2 * (h[1] + h[2]) - h[2] ** 2 / (2 * (h[2] + h[3]))))
    c.append((3 * ((a[3] - a[2]) / h[3] - (a[2] - a[1]) / h[2]) - c[1] * h[2]) / (2 * (h[2] + h[3])))
    c.append(0)

    b = [0]
    d = [0]
    for i in range(1, 4):
        d.append((c[i] - c[i-1]) / (3 * h[i]))
        b.append((a[i] - a[i-1]) / h[i] + h[i] * (2 * c[i] + c[i-1]) / 3)

    if x[0] <= arg <= x[1]:
        return a[1] + b[1] * (arg - x[1]) + c[1] * (arg - x[1]) ** 2 + d[1] * (arg - x[1]) ** 3
    elif x[1] < arg <= x[2]:
        return a[2] + b[2] * (arg - x[2]) + c[2] * (arg - x[2]) ** 2 + d[2] * (arg - x[2]) ** 3
    elif x[2] < arg <= x[3]:
        return a[3] + b[3] * (arg - x[3]) + c[3] * (arg - x[3]) ** 2 + d[3] * (arg - x[3]) ** 3


def err(arg, approximating_func):
    return fabs(func(arg) - approximating_func(arg))



def plot():
    func_label = "5 * x * ((5 * pi + 2 * x) ** (-1 / 4))"
    linear_label = "Линейный сплайн"
    parabolic_label = "Пароболический сплайн"
    cubic_label = "Кубический сплайн"

    space = np.linspace(1, 3, num=300)

    plt.figure(0)
    plt.grid()
    plt.title("Исходная функция")
    all_space = np.linspace(0, 6, num=300)
    plt.plot(all_space, [func(x) for x in all_space], label=func_label)
    plt.plot(space, [linear_spline(x) for x in space], label=linear_label)
    plt.plot(space, [parabolic_spline_build_in(x) for x in space], label=parabolic_label)
    plt.plot(space, [cubic_spline(x) for x in space], label=cubic_label)
    plt.legend()

    plt.figure(1)
    plt.grid()
    plt.title("Сплайны")
    plt.plot(space, [func(x) for x in space], label=func_label)
    plt.plot(space, [linear_spline(x) for x in space], label=linear_label)
    plt.plot(space, [parabolic_spline_build_in(x) for x in space], label=parabolic_label)
    plt.plot(space, [cubic_spline(x) for x in space], label=cubic_label)

 #   plt.plot(space, [parabolic_spline(x) for x in space], label="new parabolic spline")

    plt.legend()

    plt.figure(2)
    plt.grid()
    plt.title("Абсолютная погрешность")
    plt.plot(space, [err(x, linear_spline) for x in space], label=linear_label)
    plt.plot(space, [err(x, parabolic_spline_build_in) for x in space], label=parabolic_label)
    plt.plot(space, [err(x, cubic_spline) for x in space], label=cubic_label)
    plt.plot(space, [err(x, newton) for x in space], label="Многочлен Ньютона")
    plt.legend()

    plt.show()


plot()

x_i = input("Ввдедите x в пределах [1, 3] для проверки>")
print("Вычисление исходной функции:           ", func(float(x_i)))
print("Вычисление по линейному сплайну:       ", linear_spline(float(x_i)))
print("Вычисление по пароболическому сплайну: ", parabolic_spline_build_in(float(x_i)))
print("Вычисление по кубическому сплайну:     ", cubic_spline(float(x_i)))
print("Вычисление по многочлену Ньютона:      ", newton(float(x_i)))
